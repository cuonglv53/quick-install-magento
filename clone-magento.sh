#!/bin/bash

MAGENTO_ROOT_DIR=$2
mysql_root_pass=$4


sudo rm -rf ${MAGENTO_ROOT_DIR}
sudo mkdir -pv ${MAGENTO_ROOT_DIR}

sudo chown -R ${USER}:www-data ${MAGENTO_ROOT_DIR}

cd ${MAGENTO_ROOT_DIR}

if [ -z $3 ]; then
	composer create-project --repository=https://repo.magento.com/ magento/project-community-edition .
else
	composer create-project --repository=https://repo.magento.com/ magento/project-community-edition:$3 .
fi

find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +
find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +
chown -R :www-data .
chmod u+x bin/magento


ok() { echo -e '\e[32m'$1'\e[m'; } # Green
database=${1//./}
database=${database//com/}
datapass=${database}
datauser=${database}

#create database
echo "start create database"
RESULT=`sudo mysqlshow -uroot -p${mysql_root_pass} ${database} | grep -v Wildcard | grep -o ${database}`

if [[ "$RESULT" != "$database" ]]; then
    Q1="CREATE DATABASE IF NOT EXISTS $database;"
    sudo mysql -uroot -p${mysql_root_pass} -e "${Q1}"
    ok "Database $database created"
else
	ok "Database $database exit"
fi

echo "start create user"
RESULT="$(sudo mysql -uroot -p${mysql_root_pass} -sse "SELECT EXISTS(SELECT 1 FROM mysql.user WHERE user = '$datauser')")"
if [[ "$RESULT" != "1" ]]; then
	Q2="CREATE USER '$datauser'@'localhost' IDENTIFIED BY '$datapass';"
	Q3="GRANT ALL ON *.* TO '$datauser'@'localhost' IDENTIFIED BY '$datapass';"
	Q4="FLUSH PRIVILEGES;"
	SQL="${Q2}${Q3}${Q4}"
	sudo mysql -uroot -p${mysql_root_pass} -e "$SQL"
	if [[ $? -eq 0 ]] ; then
		ok "User $datauser created with a password $datapass"
	fi
else
	ok "User $datauser exit"
fi

php bin/magento setup:install \
    --base-url=http://$1 \
    --db-host=localhost \
    --db-name=${database} \
    --db-user=${datauser} \
    --db-password=${datapass} \
    --backend-frontname=admin \
    --admin-firstname=admin \
    --admin-lastname=admin \
    --admin-email=admin@admin.com \
    --admin-user=admin \
    --admin-password=admin123 \
    --language=en_US \
    --currency=USD \
    --timezone=America/Chicago \
    --use-rewrites=1