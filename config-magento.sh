#!/bin/bash

cd $2
php bin/magento config:set web/secure/base_url https://$1
php bin/magento config:set web/secure/use_in_frontend 1

php bin/magento cache:clean
exit